# How to use #

## On MAC ##

* Go to ~/Library/Application Support/Sublime Text 3/Packages/User/
* Clone this repo: git clone git@bitbucket.org:sronly/django-sublime3-snippets.git django
* Restart Sublime Text 3

## On LINUX ##

* Go to ~/.config/sublime-text-3/Packages/User
* Clone this repo: git clone git@bitbucket.org:sronly/django-sublime3-snippets.git django
* Restart Sublime Text 3

## On Windows ##

* Go to C:\Users\**<YOUR USER HERE>**\AppData\Roaming\Sublime Text 3\Packages\User\
* Clone this repo: git clone git@bitbucket.org:sronly/django-sublime3-snippets.git django
* Restart Sublime Text 3
